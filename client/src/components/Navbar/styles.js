import { makeStyles } from '@material-ui/core/styles';
export default makeStyles(() => ({
    appBar: {
        borderRadius: 15,
        margin: '30px 0',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'auto',
        height: "80px"
    },
    brandContainer:{
        position: 'absolute',
        left: '50px'
    },
    heading: {
        color: 'rgba(0,183,255, 1)',
        marginLeft: '100px'
    },
    image: {
        marginLeft: '15px',
    },
    log:{
        marginLeft:'700px'

    },
    profile:{
        display:'block',
        float:'left'
    },
    btn:{
       marginLeft: '900px'
    }
}));
